using System;
using System.Data.Entity;
using System.Linq;
using System.Data.Entity.Infrastructure;

namespace Day06People_WSServer_DB
{
    public class PeopleDbContext : DbContext
    {

        public PeopleDbContext()
            : base("name=PeopleDbContext")
        {
        }

         public virtual DbSet<Person> People { get; set; }

    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}