﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Day06People_WSServer_DB
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IPeopleService
    {
        [FaultContract(typeof(ApiFault))] // allows us to throw IOE back to client
        [OperationContract]
        List<Person> GetAllPeople();

        [FaultContract(typeof(ApiFault))] // allows us to throw IOE back to client
        [OperationContract]
        Person GetPerson(int id); // may not be necessary for the client

        [FaultContract(typeof(ApiFault))] // allows us to throw IOE back to client
        [OperationContract]
        void AddPerson(Person person);

        [FaultContract(typeof(ApiFault))] // allows us to throw IOE back to client
        [OperationContract]
        void UpdatePerson(Person person);

        [FaultContract(typeof(ApiFault))] // allows us to throw IOE back to client
        [OperationContract]
        void DeletePerson(int id);
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class Person
    {
        [DataMember]
        [Key]
        public int Id { get; set; }


        [DataMember]
        [Required]
        public string Name { get; set; }


        [DataMember]
        [Required]
        public int Age { get; set; }

    }

    [DataContract]
    [Serializable()]
    class ApiFault
    {
        public Exception Inner { get; set; }
        public ApiFault()
        {
        }

        public ApiFault( Exception inner)
        {
            Inner = inner;
        }
    }

}
