﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Day06People_WSServer_DB
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class PeopleService : IPeopleService
    {
        public void AddPerson(Person person)
        {
            try
            {
                using (var ctx = new PeopleDbContext())
                {
                    ctx.People.Add(person);
                    ctx.SaveChanges();
                }
            }
            catch (SystemException ex)
            {
                // TODO: Log it on the server side
                Console.WriteLine(ex.Message);
                throw new FaultException<ApiFault>(new ApiFault(ex));
            }
        }

        public void DeletePerson(int id)
        {
            try
            {
                using (var ctx = new PeopleDbContext())
                {
                    Person person = ctx.People.Where(p => p.Id == id).FirstOrDefault();
                    ctx.People.Remove(person);
                    ctx.SaveChanges();
                }
            }
            catch (SystemException ex)
            {
                // TODO: Log it on the server side
                Console.WriteLine(ex.Message);
                throw new FaultException<ApiFault>(new ApiFault(ex));
            }
        }

        public List<Person> GetAllPeople()
        {
            try
            {
                using (var ctx = new PeopleDbContext())
                {
                    return ctx.People.ToList();
                }
            }
            catch (SystemException ex)
            {
                // TODO: Log it on the server side
                Console.WriteLine(ex.Message);
                throw new FaultException<ApiFault>(new ApiFault(ex));
            }
        }

        public Person GetPerson(int id)
        {
            try
            {
                using (var ctx = new PeopleDbContext())
                {
                    return ctx.People.Where(p => p.Id == id).FirstOrDefault();
                }
            }
            catch (SystemException ex)
            {
                // TODO: Log it on the server side
                Console.WriteLine(ex.Message);
                throw new FaultException<ApiFault>(new ApiFault(ex));
            }
        }

        public void UpdatePerson(Person person)
        {
            try
            {
                using (var ctx = new PeopleDbContext())
                {
                    Person personToUpdate = ctx.People.Where(p => p.Id == person.Id).FirstOrDefault();
                    personToUpdate.Name = person.Name;
                    personToUpdate.Age = person.Age;
                    ctx.SaveChanges();
                }
            }
            catch (SystemException ex)
            {
                // TODO: Log it on the server side
                Console.WriteLine(ex.Message);
                throw new FaultException<ApiFault>(new ApiFault(ex));
            }
        }
    }
}
