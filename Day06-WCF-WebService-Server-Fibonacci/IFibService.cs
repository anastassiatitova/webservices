﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Day06_WCF_WebService_Server_Fibonacci
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IFibService
    {
        [FaultContract(typeof(InvalidOperationException))] // allows us to throw IOE back to client
        [OperationContract]
        long GetNthFibonacci_Rec(int n);

    }
        
}
