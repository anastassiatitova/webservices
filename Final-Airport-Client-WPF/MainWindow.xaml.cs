﻿
using Final_Airport_Client_WPF.AirportService1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Final_Airport_Client_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        AirportService1.AirportServiceClient client;
        Airport selectedAirport;
        public MainWindow()
        {
            InitializeComponent();

            try
            {
                client = new AirportService1.AirportServiceClient();
            }
            catch (TimeoutException exception)
            {
                Console.WriteLine("Got {0}", exception.GetType());
                this.Close();
            }
            catch (CommunicationException exception)
            {
                Console.WriteLine("Got {0}", exception.GetType());
                this.Close();
            }
            RefreshList();
        }

        private void ButtonAddAirport(object sender, RoutedEventArgs e)
        {
            string city = tbCity.Text;
            if (String.IsNullOrWhiteSpace(city))
            {
                MessageBox.Show("City should not be empty");
                return;
            }
            if (city.Length < 1 || city.Length > 20) // validate city
            {
                MessageBox.Show("City should be 1-20 characters long", "Error message");
                return;
            }
            string code = tbCode.Text;
            if (String.IsNullOrWhiteSpace(code))
            {
                MessageBox.Show("Code should not be empty");
                return;
            }
            if (code.Length != 3 || code.Length != 5) // validate code
            {
                MessageBox.Show("Code should be 3 or 5 characters long", "Error message");
                return;
            }
            string pattern = @"^[0-9A-Z ']){3,5}$";
            Match m = Regex.Match(code, pattern);
            if (!m.Success)
            {
                MessageBox.Show("Code can have only capital letters and should be 3 or 5 characters long.", "Error message");
                return;
              
            }
            if (!double.TryParse(tbLatitude.Text, out double latitude))
            {
                MessageBox.Show("Latitude is invalid");
                return;
            }
            if(latitude < -90 || latitude > 90)
            {
                MessageBox.Show("Latitude should be between -90 and 90");
                return;
            }

            if (!double.TryParse(tbLongitude.Text, out double longitute))
            {
                MessageBox.Show("Longitude is invalid");
                return;
            }
            if (longitute < -180 || longitute > 180)
            {
                MessageBox.Show("Longitude should be between -180 and 180");
                return;
            }
            try
            {
                Airport p = new Airport{ City = city, Code = code, Latitude = latitude, Longitude = longitute };
                client.AddAirport(p);
                Clear();
                //refresh the list
                RefreshList();
            }
            catch (Exception  ex)
            {
                MessageBox.Show("Error message:\n" + ex.Message);
            }
           
        }

        private void ButtonFindClosestAirport(object sender, RoutedEventArgs e)
        {

        }

        private void lvAirports_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedAirport = lvAirports.SelectedItem as Airport;  // if not person selectedPerson will be null
            if (selectedAirport != null)
            {
                lblId.Content = selectedAirport.Id;
                tbCity.Text = selectedAirport.City;
                tbCode.Text = selectedAirport.Code;
                tbLatitude.Text = selectedAirport.Latitude + "";
                tbLongitude.Text = selectedAirport.Longitude + "";
            }
            else
            {
                lblId.Content = "-";
                tbCity.Text = "";
                tbCode.Text = "";
                tbLatitude.Text = "";
                tbLongitude.Text = "";
            }
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private  void RefreshList()
        {
            try
            {
                lvAirports.ItemsSource = client.GetAllAirports().ToList();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error message:\n" + ex.Message);
            }
        }
        public void Clear()
        {
            tbCity.Text = "";
            tbCode.Text = "";
            tbLatitude.Text = "";
            tbLongitude.Text = "";
        }
    }
}
