﻿using StreamJsonRpc;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Day07_JsonRpc_Client
{
    class Program
    {
        static async Task Main()
        {
            Console.WriteLine("Connecting to server...");
            using (var stream = new NamedPipeClientStream(".", "StreamJsonRpcSamplePipe", PipeDirection.InOut, PipeOptions.Asynchronous))
            {
                await stream.ConnectAsync();
                Console.WriteLine("Connected. Sending request...");
                var jsonRpc = JsonRpc.Attach(stream);
                // now I can make an RPC call
                int sum = await jsonRpc.InvokeAsync<int>("Add", 3, 5);
                Console.WriteLine($"3 + 5 = {sum}");
                Console.WriteLine("Terminating stream...");
                Console.WriteLine("Press any key");
                Console.ReadKey();
            }
        }

        /* the full-of-async version from the original documentation */
        /*
        static async Task Main(string[] args)
        {
            Console.WriteLine("Connecting to server...");
            using (var stream = new NamedPipeClientStream(".", "StreamJsonRpcSamplePipe", PipeDirection.InOut, PipeOptions.Asynchronous))
            {
                await stream.ConnectAsync();
                await ActAsRpcClientAsync(stream);
                Console.WriteLine("Terminating stream...");
            }
        }

        private static async Task ActAsRpcClientAsync(Stream stream)
        {
            Console.WriteLine("Connected. Sending request...");
            var jsonRpc = JsonRpc.Attach(stream);
            int sum = await jsonRpc.InvokeAsync<int>("Add", 3, 5);
            Console.WriteLine($"3 + 5 = {sum}");
            Console.WriteLine("Press any key to finish");
            Console.ReadKey();
        }
        */
    }
}
