﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day06_WCF_WebService_Client_Fibonacci
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        ServiceReference1.FibServiceClient client;
        public MainWindow()
        {
            InitializeComponent();

            // TODO: make sure this does not throw any exceptions and if it does we must handle them
            try
            {
                client = new ServiceReference1.FibServiceClient();
            }
            catch (TimeoutException exception)
            {
                Console.WriteLine("Got {0}", exception.GetType());
                client.Abort();
            }
            catch (CommunicationException exception)
            {
                Console.WriteLine("Got {0}", exception.GetType());
                client.Abort();
            }

        }

        private void btnCompute_Click(object sender, RoutedEventArgs e)
        {
            if (!int.TryParse(tbNumber.Text, out int n))
            {
                MessageBox.Show("Invalid number value");
            }
            // call the API and show the result
            try
            {
                // TODO: handle exceptions / errors caused by the call
                long result =  client.GetNthFibonacci_Rec(n); // TODO: TimeOutException to be handled
                lblResult.Content = "" + result;
            }
            catch (System.ServiceModel.FaultException<InvalidOperationException> ex)
            {
                InvalidOperationException actualEx = ex.Detail; // unwrap the actual exception
                MessageBox.Show("API Error: " + actualEx.Message);
            }
            catch (System.ServiceModel.FaultException ex)
            {
                MessageBox.Show("API Error (exception not wrapped?):\n" + ex.Message);
            }
            catch (System.ServiceModel.EndpointNotFoundException ex)
            {
                MessageBox.Show("API Error: unable to contact the server (it is runnning?)\n" + ex.Message);
            }
        }
    }
}
