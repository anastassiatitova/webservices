﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Day06People_WSClient_WPF.PeopleServiceReference {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Person", Namespace="http://schemas.datacontract.org/2004/07/Day06People_WSServer_DB")]
    [System.SerializableAttribute()]
    public partial class Person : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int AgeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int IdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NameField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Age {
            get {
                return this.AgeField;
            }
            set {
                if ((this.AgeField.Equals(value) != true)) {
                    this.AgeField = value;
                    this.RaisePropertyChanged("Age");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id {
            get {
                return this.IdField;
            }
            set {
                if ((this.IdField.Equals(value) != true)) {
                    this.IdField = value;
                    this.RaisePropertyChanged("Id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name {
            get {
                return this.NameField;
            }
            set {
                if ((object.ReferenceEquals(this.NameField, value) != true)) {
                    this.NameField = value;
                    this.RaisePropertyChanged("Name");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ApiFault", Namespace="http://schemas.datacontract.org/2004/07/Day06People_WSServer_DB")]
    [System.SerializableAttribute()]
    public partial class ApiFault : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="PeopleServiceReference.IPeopleService")]
    public interface IPeopleService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPeopleService/GetAllPeople", ReplyAction="http://tempuri.org/IPeopleService/GetAllPeopleResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(Day06People_WSClient_WPF.PeopleServiceReference.ApiFault), Action="http://tempuri.org/IPeopleService/GetAllPeopleApiFaultFault", Name="ApiFault", Namespace="http://schemas.datacontract.org/2004/07/Day06People_WSServer_DB")]
        Day06People_WSClient_WPF.PeopleServiceReference.Person[] GetAllPeople();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPeopleService/GetAllPeople", ReplyAction="http://tempuri.org/IPeopleService/GetAllPeopleResponse")]
        System.Threading.Tasks.Task<Day06People_WSClient_WPF.PeopleServiceReference.Person[]> GetAllPeopleAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPeopleService/GetPerson", ReplyAction="http://tempuri.org/IPeopleService/GetPersonResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(Day06People_WSClient_WPF.PeopleServiceReference.ApiFault), Action="http://tempuri.org/IPeopleService/GetPersonApiFaultFault", Name="ApiFault", Namespace="http://schemas.datacontract.org/2004/07/Day06People_WSServer_DB")]
        Day06People_WSClient_WPF.PeopleServiceReference.Person GetPerson(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPeopleService/GetPerson", ReplyAction="http://tempuri.org/IPeopleService/GetPersonResponse")]
        System.Threading.Tasks.Task<Day06People_WSClient_WPF.PeopleServiceReference.Person> GetPersonAsync(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPeopleService/AddPerson", ReplyAction="http://tempuri.org/IPeopleService/AddPersonResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(Day06People_WSClient_WPF.PeopleServiceReference.ApiFault), Action="http://tempuri.org/IPeopleService/AddPersonApiFaultFault", Name="ApiFault", Namespace="http://schemas.datacontract.org/2004/07/Day06People_WSServer_DB")]
        void AddPerson(Day06People_WSClient_WPF.PeopleServiceReference.Person person);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPeopleService/AddPerson", ReplyAction="http://tempuri.org/IPeopleService/AddPersonResponse")]
        System.Threading.Tasks.Task AddPersonAsync(Day06People_WSClient_WPF.PeopleServiceReference.Person person);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPeopleService/UpdatePerson", ReplyAction="http://tempuri.org/IPeopleService/UpdatePersonResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(Day06People_WSClient_WPF.PeopleServiceReference.ApiFault), Action="http://tempuri.org/IPeopleService/UpdatePersonApiFaultFault", Name="ApiFault", Namespace="http://schemas.datacontract.org/2004/07/Day06People_WSServer_DB")]
        void UpdatePerson(Day06People_WSClient_WPF.PeopleServiceReference.Person person);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPeopleService/UpdatePerson", ReplyAction="http://tempuri.org/IPeopleService/UpdatePersonResponse")]
        System.Threading.Tasks.Task UpdatePersonAsync(Day06People_WSClient_WPF.PeopleServiceReference.Person person);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPeopleService/DeletePerson", ReplyAction="http://tempuri.org/IPeopleService/DeletePersonResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(Day06People_WSClient_WPF.PeopleServiceReference.ApiFault), Action="http://tempuri.org/IPeopleService/DeletePersonApiFaultFault", Name="ApiFault", Namespace="http://schemas.datacontract.org/2004/07/Day06People_WSServer_DB")]
        void DeletePerson(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPeopleService/DeletePerson", ReplyAction="http://tempuri.org/IPeopleService/DeletePersonResponse")]
        System.Threading.Tasks.Task DeletePersonAsync(int id);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IPeopleServiceChannel : Day06People_WSClient_WPF.PeopleServiceReference.IPeopleService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class PeopleServiceClient : System.ServiceModel.ClientBase<Day06People_WSClient_WPF.PeopleServiceReference.IPeopleService>, Day06People_WSClient_WPF.PeopleServiceReference.IPeopleService {
        
        public PeopleServiceClient() {
        }
        
        public PeopleServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public PeopleServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public PeopleServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public PeopleServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public Day06People_WSClient_WPF.PeopleServiceReference.Person[] GetAllPeople() {
            return base.Channel.GetAllPeople();
        }
        
        public System.Threading.Tasks.Task<Day06People_WSClient_WPF.PeopleServiceReference.Person[]> GetAllPeopleAsync() {
            return base.Channel.GetAllPeopleAsync();
        }
        
        public Day06People_WSClient_WPF.PeopleServiceReference.Person GetPerson(int id) {
            return base.Channel.GetPerson(id);
        }
        
        public System.Threading.Tasks.Task<Day06People_WSClient_WPF.PeopleServiceReference.Person> GetPersonAsync(int id) {
            return base.Channel.GetPersonAsync(id);
        }
        
        public void AddPerson(Day06People_WSClient_WPF.PeopleServiceReference.Person person) {
            base.Channel.AddPerson(person);
        }
        
        public System.Threading.Tasks.Task AddPersonAsync(Day06People_WSClient_WPF.PeopleServiceReference.Person person) {
            return base.Channel.AddPersonAsync(person);
        }
        
        public void UpdatePerson(Day06People_WSClient_WPF.PeopleServiceReference.Person person) {
            base.Channel.UpdatePerson(person);
        }
        
        public System.Threading.Tasks.Task UpdatePersonAsync(Day06People_WSClient_WPF.PeopleServiceReference.Person person) {
            return base.Channel.UpdatePersonAsync(person);
        }
        
        public void DeletePerson(int id) {
            base.Channel.DeletePerson(id);
        }
        
        public System.Threading.Tasks.Task DeletePersonAsync(int id) {
            return base.Channel.DeletePersonAsync(id);
        }
    }
}
