﻿using Day06People_WSClient_WPF.PeopleServiceReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day06People_WSClient_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        PeopleServiceReference.IPeopleService client;
        Person selectedPerson;

        public MainWindow()
        {
            InitializeComponent();
            try
            {
                client = new PeopleServiceReference.PeopleServiceClient();
            }
            catch (TimeoutException exception)
            {
                Console.WriteLine("Got {0}", exception.GetType());
                this.Close();
            }
            catch (CommunicationException exception)
            {
                Console.WriteLine("Got {0}", exception.GetType());
                this.Close();
            }
            try
            {
                lvPeople.ItemsSource = client.GetAllPeople().ToList();
            }
            catch (System.ServiceModel.FaultException<ApiFault> ex)
            {
                ApiFault fault = ex.Detail; // unwrap the actual exception
                MessageBox.Show("API Error: " + fault);
            }
            catch (System.ServiceModel.FaultException ex)
            {
                MessageBox.Show("API Error (exception not wrapped?):\n" + ex.Message);
            }
            catch (System.ServiceModel.EndpointNotFoundException ex)
            {
                MessageBox.Show("API Error: unable to contact the server (it is runnning?)\n" + ex.Message);
            }
        }

        private void ButtonAddPerson(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
     /*       if (String.IsNullOrWhiteSpace(name))
            {
                MessageBox.Show("Name should not be empty");
                return;
            }
            if (name.Length < 2 || name.Length > 100) // validate name
            {
                MessageBox.Show("Name should be 2-100 characters long", "Error message");
                return;
            } */
            if (!int.TryParse(tbAge.Text, out int age))
            {
                MessageBox.Show("Age invalid");
                return;
            }
            try
            {
                Person p = new Person { Name = name, Age = age };
                client.AddPerson(p);
                Clear();
                //refresh the list
                lvPeople.ItemsSource = client.GetAllPeople().ToList();
            }
            catch (System.ServiceModel.FaultException<ApiFault> ex)
            {
                ApiFault fault = ex.Detail; // unwrap the actual exception
                MessageBox.Show("API Error: " + fault);
            }
            catch (System.ServiceModel.FaultException ex)
            {
                MessageBox.Show("API Error (exception not wrapped?):\n" + ex.Message);
            }
            catch (System.ServiceModel.EndpointNotFoundException ex)
            {
                MessageBox.Show("API Error: unable to contact the server (it is runnning?)\n" + ex.Message);
            }
        }

        private void ButtonUpdatePerson(object sender, RoutedEventArgs e)
        {
            try
            {
                selectedPerson.Name = tbName.Text;
          /*      if (String.IsNullOrWhiteSpace(selectedPerson.Name))
                {
                    MessageBox.Show("Name should not be empty");
                    return;
                }
                if ((selectedPerson.Name.Length < 2) || (selectedPerson.Name.Length > 100)) // validate name
                {
                    MessageBox.Show("Name should be 2-100 characters long", "Error message");
                    return;
                } */
                if (!int.TryParse(tbAge.Text, out int age))
                {
                    MessageBox.Show("Age invalid");
                    return;
                }
                selectedPerson.Age = age;
                client.UpdatePerson(selectedPerson);
                Clear();
                //refresh the list
                lvPeople.ItemsSource = client.GetAllPeople().ToList();
            }
            catch (System.ServiceModel.FaultException<ApiFault> ex)
            {
                ApiFault fault = ex.Detail; // unwrap the actual exception
                MessageBox.Show("API Error: " + fault);
            }
            catch (System.ServiceModel.FaultException ex)
            {
                MessageBox.Show("API Error (exception not wrapped?):\n" + ex.Message);
            }
            catch (System.ServiceModel.EndpointNotFoundException ex)
            {
                MessageBox.Show("API Error: unable to contact the server (it is runnning?)\n" + ex.Message);
            }
        }

        private void ButtonDeletePerson(object sender, RoutedEventArgs e)
        {
            // See if the user really wants to delete a Todo
            string msg = "Do you want to delete the record?\n" + selectedPerson.Name;
            MessageBoxResult result = MessageBox.Show(msg, "My App", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
            if (result == MessageBoxResult.OK)
            {

                try
                {
                    client.DeletePerson(selectedPerson.Id);
                    Clear();
                    //refresh the list
                    lvPeople.ItemsSource = client.GetAllPeople().ToList();
                }
                catch (System.ServiceModel.FaultException<ApiFault> ex)
                {
                    ApiFault fault = ex.Detail; // unwrap the actual exception
                    MessageBox.Show("API Error: " + fault);
                }
                catch (System.ServiceModel.FaultException ex)
                {
                    MessageBox.Show("API Error (exception not wrapped?):\n" + ex.Message);
                }
                catch (System.ServiceModel.EndpointNotFoundException ex)
                {
                    MessageBox.Show("API Error: unable to contact the server (it is runnning?)\n" + ex.Message);
                }
            }
        }

        private void lvPeopleSelection(object sender, SelectionChangedEventArgs e)
        {
            selectedPerson = lvPeople.SelectedItem as Person;  // if not person selectedPerson will be null
            if (selectedPerson != null)
            {
                lblId.Content = selectedPerson.Id;
                tbName.Text = selectedPerson.Name;
                tbAge.Text = selectedPerson.Age + "";
            }
            else
            {
                lblId.Content = "-";
                tbName.Text = "";
                tbAge.Text = "";
            }
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        public void Clear()
        {
            tbName.Text = "";
            tbAge.Text = "";
        }
    }
}
