﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06_WCF_WebService_Client_HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();
            String result = client.GetData(777);
            Console.WriteLine("Result is  " + result);
            Console.ReadKey();
        }
    }
}
