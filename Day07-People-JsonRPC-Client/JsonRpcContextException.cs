﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StreamJsonRpc;

namespace Day07_People_JsonRPC_Client
{
    class JsonRpcContextException : Exception
    {
        public Exception Inner { get; set; }
        public JsonRpcContextException()
        {
        }

        public JsonRpcContextException(Exception inner)
        {
            Inner = inner;
        }
    }
}
