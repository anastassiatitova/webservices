﻿using StreamJsonRpc;
using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07_People_JsonRPC_Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        JsonRpc jsonRpc;
        Person selectedPerson;

        public MainWindow()
        {
            InitializeComponent();
            try
            {
                var stream = new NamedPipeClientStream(".", "StreamJsonRpcSamplePipe", PipeDirection.InOut, PipeOptions.Asynchronous);
                stream.ConnectAsync().Wait();
                jsonRpc = JsonRpc.Attach(stream);
                RefreshList();
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            catch (RemoteRpcException ex)
            {
                Console.WriteLine(ex.Message);
                MessageBox.Show(ex.Message);
            }
        }

        private void lvPeopleSelection(object sender, SelectionChangedEventArgs e)
        {
            selectedPerson = lvPeople.SelectedItem as Person;  // if not person selectedPerson will be null
            if (selectedPerson != null)
            {
                lblId.Content = selectedPerson.Id;
                tbName.Text = selectedPerson.Name;
                tbAge.Text = selectedPerson.Age + "";
            }
            else
            {
                lblId.Content = "-";
                tbName.Text = "";
                tbAge.Text = "";
            }
        }

        private void ButtonAddPerson(object sender, RoutedEventArgs e)
        {
            try
            {
                string name = tbName.Text;
                if (String.IsNullOrWhiteSpace(name))
                {
                    MessageBox.Show("Name should not be empty");
                    return;
                }
                if (name.Length < 2 || name.Length > 100) // validate name
                {
                    MessageBox.Show("Name should be 2-100 characters long", "Error message");
                    return;
                }
                if (!int.TryParse(tbAge.Text, out int age))
                {
                    MessageBox.Show("Age invalid");
                    return;
                }

                Person p = new Person { Name = name, Age = age };
                jsonRpc.InvokeAsync<Person>("AddPerson", p).Wait();
                Clear();
                //refresh the list
                RefreshList();
            }
            catch (RemoteRpcException ex)
            {
                // Console.WriteLine(ex);
                MessageBox.Show(ex.Message);
            }
        }

        private void ButtonUpdatePerson(object sender, RoutedEventArgs e)
        {
            try
            {
                selectedPerson.Name = tbName.Text;
                if (String.IsNullOrWhiteSpace(selectedPerson.Name))
                {
                    MessageBox.Show("Name should not be empty");
                    return;
                }
                if ((selectedPerson.Name.Length < 2) || (selectedPerson.Name.Length > 100)) // validate name
                {
                    MessageBox.Show("Name should be 2-100 characters long", "Error message");
                    return;
                }
                if (!int.TryParse(tbAge.Text, out int age))
                {
                    MessageBox.Show("Age invalid");
                    return;
                }
                selectedPerson.Age = age;
                jsonRpc.InvokeAsync<Person>("UpdatePerson", selectedPerson).Wait();
                Clear();
                //refresh the list
                RefreshList();
            }
            catch (RemoteRpcException ex)
            {
                // Console.WriteLine(ex);
                MessageBox.Show(ex.Message);
            }
        }

        private void ButtonDeletePerson(object sender, RoutedEventArgs e)
        {
            try
            {
                // See if the user really wants to delete a Todo
                string msg = "Do you want to delete the record?\n" + selectedPerson.Name;
                MessageBoxResult result = MessageBox.Show(msg, "My App", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                if (result == MessageBoxResult.OK)
                {
                    jsonRpc.InvokeAsync<Person>("DeletePerson", selectedPerson.Id).Wait();
                    Clear();
                    //refresh the list
                    RefreshList();
                }
            }
            catch (RemoteRpcException ex)
            {
                // Console.WriteLine(ex);
                MessageBox.Show(ex.Message);
            }
        }

        public void RefreshList()
        {
            try
            {
                lvPeople.ItemsSource = jsonRpc.InvokeAsync<List<Person>>("GetAllPeople").Result;
            }
            catch (RemoteRpcException ex)
            {
                // Console.WriteLine(ex);
                MessageBox.Show(ex.Message);
            }
        }
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        public void Clear()
        {
            tbName.Text = "";
            tbAge.Text = "";
        }
    }
}
