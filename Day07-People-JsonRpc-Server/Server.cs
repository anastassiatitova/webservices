﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day07_People_JsonRpc_Server
{
    class Server
    {

        public void AddPerson(Person person)
        {
            try
            {
                using (var ctx = new PeopleDbContext())
                {
                    ctx.People.Add(person);
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                // TODO: Log it on the server side
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        public void DeletePerson(int id)
        {
            try
            {
                using (var ctx = new PeopleDbContext())
                {
                    Person person = ctx.People.Where(p => p.Id == id).FirstOrDefault();
                    ctx.People.Remove(person);
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        public List<Person> GetAllPeople()
        {
            try
            {
                using (var ctx = new PeopleDbContext())
                {
                    return ctx.People.ToList();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        public Person GetPerson(int id)
        {
            try
            {
                using (var ctx = new PeopleDbContext())
                {
                    return ctx.People.Where(p => p.Id == id).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                // TODO: Log it on the server side
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        public void UpdatePerson(Person person)
        {
            try
            {
                using (var ctx = new PeopleDbContext())
                {
                    Person personToUpdate = ctx.People.Where(p => p.Id == person.Id).FirstOrDefault();
                    personToUpdate.Name = person.Name;
                    personToUpdate.Age = person.Age;
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                // TODO: Log it on the server side
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }
    }
}
