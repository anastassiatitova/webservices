﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Final_Airport_Server
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IAirportService
    {

        [OperationContract]
        List<Airport> GetAllAirports();


        [OperationContract]
        Airport GetAirportById(int id);


        [OperationContract]
        void AddAirport(Airport airport);


        [OperationContract]
        Airport FindClosestAirportTo(int airportId);

        // TODO: Add your service operations here
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class Airport
    {
        [DataMember]
        [Key]
        public int Id { get; set; }


        [DataMember]
        [Required(ErrorMessage = "User Name is required")]
        [MinLength(1, ErrorMessage = "City should have at least 1 character."), MaxLength(20, ErrorMessage = "City is 20 characters long.")]
        public string City { get; set; }


        [DataMember]
        [Required]
        [Index(IsUnique = true)]
        [RegularExpression("^[0-9A-Z ']){3,5}$", ErrorMessage = "Code can have only capital letters and should be 3 or 5 characters long.")]
        public string Code { get; set; }

        [DataMember]
        [Required]
        [Range(-90, 90, ErrorMessage = "Latitude is in the range between -90 and 90.")]

        public double Latitude { get; set; }

        [DataMember]
        [Required]
        [Range(-180, 180, ErrorMessage = "Longitude is in the range between -180 and 180.")]

        public double Longitude { get; set; }

    }


}
