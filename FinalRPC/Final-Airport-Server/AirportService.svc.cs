﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Final_Airport_Server
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class AirportService : IAirportService
    {
        public void AddAirport(Airport airport)
        {
            try
            {
                using (var ctx = new AirportDbContext())
                {
                    ctx.Airports.Add(airport);
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                // TODO: Log it on the server side
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        public Airport FindClosestAirportTo(int airportId)
        {
            throw new NotImplementedException();
        }

        public Airport GetAirportById(int id)
        {
            try
            {
                using (var ctx = new AirportDbContext())
                {
                    return ctx.Airports.Where(p => p.Id == id).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                // TODO: Log it on the server side
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        public List<Airport> GetAllAirports()
        {
            try
            {
                using (var ctx = new AirportDbContext())
                {
                    return ctx.Airports.ToList();
                }
            }
            catch (Exception ex)
            {
                // TODO: Log it on the server side
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

      
    }
}
